//
//  ViewController.m
//  LearnLeanCloud
//
//  Created by zhongdian on 15/5/5.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#import "ViewController.h"
#import <AVOSCloud/AVOSCloud.h>

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *phoneNumLabel;
@property (strong, nonatomic) IBOutlet UITextField *verifyNumLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
    AVObject *gameScore = [AVObject objectWithClassName:@"GameScore"];
    [gameScore setObject:[NSNumber numberWithInt:1337] forKey:@"score"];
    [gameScore setObject:@"Steve" forKey:@"playerName"];
    [gameScore setObject:[NSNumber numberWithBool:NO] forKey:@"cheatMode"];
    [gameScore save];
    */
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)getVerifyNum:(id)sender {
    NSString *phoneNum = self.phoneNumLabel.text;
    
    [AVOSCloud requestSmsCodeWithPhoneNumber:phoneNum
                                     appName:@"某应用"
                                   operation:@"测试"
                                  timeToLive:10
                                    callback:^(BOOL succeeded, NSError *error) {
                                        // 执行结果
                                        if (succeeded) {
                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"成功发送" message:@"成功发送验证码信息" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles: nil];
                                            [av show];
                                        }
                                        else
                                        {
                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"出现错误" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
                                            [av show];
                                        }
                                        
                                    }];
}

- (IBAction)signUp:(id)sender {
    
}



@end
